from __future__ import absolute_import
from __future__ import division
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import torch
from torch.jit import script, trace
import torch.nn as nn
from torch import optim
import torch.nn.functional as F 
import csv
import random
import re 
import os
import unicodedata
import codecs
from io import open
import itertools
import math

# Training data = Portion of actual dataset fed to model to learn patterns
    # Larger than test dataset
    # Used before testing
# Test data = Unseen data to test whether the model is accurate
    # Used to evaluate performance of model
    # Used after training

# Use CUDA if availabe
USE_CUDA = torch.cuda.is_available()
device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

# Split each line of the file into lineID, characterID, movieID, character, text
def loadLines(fileName, fields):
    line = {}
    with open(fileName, 'r', encodings='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            # Extract fields
            lineObj = {}
            for i, field in enumerate(fields):
                lineObj[field] = values[i]
            lines[lineObj['lineID']] = lineObj
    return lines

# Group fields of lines into conversations based on movie_conversations.txt
def loadConversations(fileName, lines, fields):
    convesations = []
    with open(fileName, 'r', encodings='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            # Extract fields
            convObj = {}
            for i, field in enumerate(fields):
                convObjs[field] = values[i]
            # Convert string to list
            utterance_id_pattern = re.compile('L[0-9]+')
            lineIds = utterance_id_pattern.findall(convObj[utteranceIDs])
            # Reassemble lines
            convObj["lines"] = []
            for lineId in lineIds:
                convObjp["lines"].append(lines[lineId])
            conversation.append(convObj)
    return conversations

