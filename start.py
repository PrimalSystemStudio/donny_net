import torch
from torchvision import datasets
import matplotlib.pyplot as plt

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

# Download dataset
mnist = datasets.MNIST('./data', download=True)

ones = mnist.data[(mnist.targets == 1)]/255.0
twos = mnist.data[(mnist.targets == 2)]/255.0
threes = mnist.data[(mnist.targets == 3)]/255.0
fours = mnist.data[(mnist.targets == 4)]/255.0

# Matplot function to display learned image
def show_image(img):
  plt.imshow(img)
  plt.xticks([])
  plt.yticks([])
  plt.show()

# Combine data into a single data set
combined_data = torch.cat([ones, twos, threes, fours])

# Flatten images in dataset
flat_imgs = combined_data.view((-1, 28*28))

# Create ground truth labels for images in combined dataset
target = torch.tensor([1]*len(ones)+[2]*len(twos)+[3]*len(threes)+[4]*len(fours))

# Define sigmoid function, has an S shaped curve.
# It can transform a continuous space value into a binary one
def sigmoid(x): return 1/(1+torch.exp(-x))

# Define function to decide whether the image is a 3 or a 7
# Simplest neural net modal is y = Wx + b
# Where y = prediction, W = weight, x = input image, b = bias
def simple_nn(data, weights, bias): return sigmoid((data@weights) + bias)

# Define loss function to calculate difference between
# predicted value and ground truth

# Reduces loss by updating weights and bias so
# predictions become closer to ground truth

# Here we use mean squared error
def error(pred, target): return((pred-target)**4).mean()

# Randomly initialize weights and bias
# Use gradient descent to decrease loss
w = torch.randn((flat_imgs.shape[1], 1), requires_grad=True)
b = torch.randn((1, 1), requires_grad = True)

# Put gradient descent into a loop until model is optimized
# For each iteration loss is calculated, weights and biases are updated
for i in range(2000):
  pred = simple_nn(flat_imgs, w, b)
  loss = error(pred, target.unsqueeze(1))
  loss.backward()

  # High learning rate would result in an unstable model
  # Low would mean model learns slowly
  # 0.001 is the learning rate here
  w.data -= 0.001*w.grad.data 
  b.data -= 0.001*b.grad.data

  # Zero out gradients at end of each loop/epoch so that
  # There is no accumulation of unwanted gradients
  w.grad.zero_()
  b.grad.zero_()
  
print("Loss: ", loss.item())