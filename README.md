Starter neural net for learning

Intended to create a custom virtual assistant for myself.
Others could make their own by cloning this repository, this guaruntees privacy because I won't ever have your information to process it.
Perhaps this could be streamlined in the future, not sure where this will go (if anywhere at all)

Tools collected:
- Glitch SQLite database https://glitch.com/edit/#!/butternut-small-saga?path=README.md%3A1%3A0
- Simple chat UI https://glitch.com/edit/#!/secure-chat-remix?path=Readme.md%3A1%3A0
- Continuous learning framework https://github.com/automl/Auto-PyTorch
- Simple chatbot https://pytorch.org/tutorials/beginner/chatbot_tutorial.html